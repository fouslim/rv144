# RV144: source code #

## List of figures:
 
[Fig. 1](#markdown-header-fig-1), [Fig. 2](#markdown-header-fig-2), [Fig. 3](#markdown-header-fig-3), [Fig. 4](#markdown-header-fig-4), [Fig. 5a-b](#markdown-header-fig-5a-b), [Fig. 5c-d](#markdown-header-fig-5c-d), [Table 1](#markdoww-header-table-1)

### Fig. 1
![Fig. 1](Fig1/20150201_RV144pilot.Fig1.png)
Fig. 1: [R code [PDF]](Fig1/20160510_RV144pilot.Fig1.code.pdf), [Input file [RData]](https://storage.googleapis.com/rv144pilot_20140428/genesets_analysis/rv144pilot.gsSetVehSubstracted.RData) 

### Fig. 2
![Fig. 2](https://storage.googleapis.com/rv144_20140428/Fig2/20150201_RV144pilot.Fig2.png)

### Fig. 3
![Fig. 3](https://storage.googleapis.com/rv144_20140428/Fig3/20150201_RV144pilot.Fig3.png)

### Fig. 4
![Fig. 4](https://storage.googleapis.com/rv144_20140428/Fig4/20150201_RV144pilot.Fig4.png)

### Fig. 5a-b
![Fig. 5 a-b](https://storage.googleapis.com/rv144_20140428/Fig5ab/20170127_RV144.Fig5ab.png)

### Fig. 5c-d
![Fig. 5 c-d](https://storage.googleapis.com/rv144_20140428/Fig5cd/20170127_RV144.Fig5cd.png)

### Table 1
![Table 1](https://storage.googleapis.com/rv144_20140428/Table1/20170809_RV144.Table1.png)
